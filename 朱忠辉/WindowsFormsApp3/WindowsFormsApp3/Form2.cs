﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            var uid = userName.Text;
            var pwd = passWord.Text;

            var rPwd = reTrypassword.Text;

            var isUidNull = string.IsNullOrEmpty(uid);
            var isPwdNull = string.IsNullOrEmpty(pwd);
            var isEqure = pwd == rPwd;
            if (!isUidNull  && !isPwdNull  && !isEqure)
            {
                MessageBox.Show("恭喜你注册成功！");
                Form1._user.Add(uid, pwd);
                this.Close();
            }
            else
            {
                MessageBox.Show("很抱歉未成功");
            }
        }
    }
}
