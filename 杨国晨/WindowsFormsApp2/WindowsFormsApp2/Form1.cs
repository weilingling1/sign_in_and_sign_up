﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
       public static readonly Dictionary<string, string> user = new Dictionary<string, string>();


        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            user.Add("aaa","111");
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                userPwd.PasswordChar = new char();
            }
            else
            {
                userPwd.PasswordChar = '*';
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //var uName = "aaa";
            //var uPwd = "111";

            var uId = userName.Text;
            var pwd = userPwd.Text;

            var u = user.Where(x=>x.Key==uId).FirstOrDefault();
            if (u.Key==uId && u.Value==pwd)
            { 
                MessageBox.Show("恭喜你，登录成功！！");
            
            }
            else
            {
                MessageBox.Show("很遗憾，登录失败，请重新登录！！");
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            form2.Show();
        }
    }
}
