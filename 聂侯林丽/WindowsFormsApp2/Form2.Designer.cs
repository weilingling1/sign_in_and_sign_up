﻿namespace WindowsFormsApp2
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.passWord = new System.Windows.Forms.TextBox();
            this.userName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnCanceIRegister = new System.Windows.Forms.Button();
            this.btunRegister = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.reTyPassWord = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // passWord
            // 
            this.passWord.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.passWord.Location = new System.Drawing.Point(289, 121);
            this.passWord.Name = "passWord";
            this.passWord.Size = new System.Drawing.Size(234, 26);
            this.passWord.TabIndex = 5;
            // 
            // userName
            // 
            this.userName.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.userName.Location = new System.Drawing.Point(289, 61);
            this.userName.Name = "userName";
            this.userName.Size = new System.Drawing.Size(234, 26);
            this.userName.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(171, 127);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "密码：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(171, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "用户名：";
            // 
            // btnCanceIRegister
            // 
            this.btnCanceIRegister.Font = new System.Drawing.Font("宋体", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnCanceIRegister.Location = new System.Drawing.Point(398, 257);
            this.btnCanceIRegister.Name = "btnCanceIRegister";
            this.btnCanceIRegister.Size = new System.Drawing.Size(125, 36);
            this.btnCanceIRegister.TabIndex = 7;
            this.btnCanceIRegister.Text = "取消注册";
            this.btnCanceIRegister.UseVisualStyleBackColor = true;
            // 
            // btunRegister
            // 
            this.btunRegister.Font = new System.Drawing.Font("宋体", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btunRegister.Location = new System.Drawing.Point(148, 257);
            this.btunRegister.Name = "btunRegister";
            this.btunRegister.Size = new System.Drawing.Size(112, 36);
            this.btunRegister.TabIndex = 8;
            this.btunRegister.Text = "注册";
            this.btunRegister.UseVisualStyleBackColor = true;
            this.btunRegister.Click += new System.EventHandler(this.btunRegister_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(171, 187);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "确认密码：";
            // 
            // reTyPassWord
            // 
            this.reTyPassWord.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.reTyPassWord.Location = new System.Drawing.Point(289, 181);
            this.reTyPassWord.Name = "reTyPassWord";
            this.reTyPassWord.Size = new System.Drawing.Size(234, 26);
            this.reTyPassWord.TabIndex = 5;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnCanceIRegister);
            this.Controls.Add(this.btunRegister);
            this.Controls.Add(this.reTyPassWord);
            this.Controls.Add(this.passWord);
            this.Controls.Add(this.userName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Name = "Form2";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox passWord;
        private System.Windows.Forms.TextBox userName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnCanceIRegister;
        private System.Windows.Forms.Button btunRegister;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox reTyPassWord;
    }
}