﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public static Dictionary<string, string> _user = new Dictionary<string, string>();

        private void Form1_Load(object sender, EventArgs e)
        {
            this.BackColor = Color.Pink;
            _user.Add("admit", "123");
        }

        private void button1_Click(object sender, EventArgs e)
        {            

            var uid = uName.Text;
            var pwd = uWord.Text;

            var res = _user.Where(x => x.Key == uid).FirstOrDefault();

            if (res.Key == uid && res.Value == pwd)
            {
                MessageBox.Show("登录成功", "恭喜！", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("登录失败", "请重新登录", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            form2.Show();
        }
    }
}
