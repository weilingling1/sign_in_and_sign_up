﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public static Dictionary<String, String> Users = new Dictionary<string, string>();

        private void button1_Click(object sender, EventArgs e)
        {
            var uid = username.Text;
            var pwd = password.Text;

            var res = Users.Where(x => x.Key == uid).FirstOrDefault();
            if(res.Key == uid && res.Value == pwd)
            {
                MessageBox.Show("登陆成功！", "恭喜", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

            }
            else
            {
                MessageBox.Show("登录失败，请先注册！", "很遗憾", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Users.Add("sa", "123");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            form2.Show();
        }
    }
}
