﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Login_Register
{
    public partial class Register : Form
    {
        public Register()
        {
            InitializeComponent();
        }

        private void 注册_Click(object sender, EventArgs e)
        {
            string newName = this.userName.Text;
            string newPwd = this.userPwd.Text;
            string newPwd_2 = this.userPwd_2.Text;
            if (!string.IsNullOrEmpty(newName))
            {
                if (!string.IsNullOrEmpty(newPwd) || !string.IsNullOrEmpty(newPwd_2))
                {
                    if (newPwd == newPwd_2)
                    {
                        MessageBox.Show("注册成功！欢迎你，" + userName.Text, "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Form1._user.Add(newName,newPwd);
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("前后密码不一致！","错误",MessageBoxButtons.OK,MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("密码不能为空！", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("用户名不能为空！","错误",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
        }

        private void 取消_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
