﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp4
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            var uid = userName.Text;
            var pwd = passWord.Text;
            var rPwd = rePassword.Text;

            var isUid = string.IsNullOrEmpty(uid);
            var isPwd = string.IsNullOrEmpty(pwd);
            var isEqure = pwd == rPwd;

            if (isUid && isPwd && isEqure)

            // if (string.IsNullOrEmpty(uid) && string.IsNullOrEmpty(pwd) && pwd == rPwd)
            {
                MessageBox.Show("恭喜您，注册成功！");
                Form1._user.Add(uid, pwd);
                this.Close();

            }
            else
            {
                MessageBox.Show("很抱歉，注册失败！");
            }
        }

        private void Register_Click(object sender, EventArgs e)
        {
            MessageBox.Show("已取消");
        }
    }
}
