﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Register : Form
    {
        public Register()
        {
            InitializeComponent();
        }

        private void userName_TextChanged(object sender, EventArgs e)
        {

        }

        private void Regis_Click(object sender, EventArgs e)
        {
            var uid = userName.Text;
            var pwd = passWord.Text;
            var rPwd = respassword.Text;

            var isUidNull = string.IsNullOrEmpty(uid);
            var isPwdNull = string.IsNullOrEmpty(pwd);
            var isEqure = pwd == rPwd;



            if ( !isUidNull && !isPwdNull && isEqure)
            {
                MessageBox.Show("注册成功", "完成",MessageBoxButtons.YesNo,MessageBoxIcon.Information);
                Form1._user.Add(uid, pwd);
                this.Close();
            }
            else
            {
                MessageBox.Show("注册失败，请重新注册", "抱歉",MessageBoxButtons.YesNo,MessageBoxIcon.Error);
            }
        }

        private void butencancel_Click(object sender, EventArgs e)
        {

        }
    }
}
