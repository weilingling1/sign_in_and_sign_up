﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            var uid = userName.Text;
            var pwd = passWord.Text;
            //var rPwd=re


            if (string.IsNullOrEmpty(uid) && string.IsNullOrEmpty(pwd))
            {
                MessageBox.Show("恭喜你，注册成功！");
                Form1._user.Add(uid, pwd);
                this.Close();

            }
            else
            {
                MessageBox.Show("很遗憾，注册成功！");
                Form1._user.Add(uid, pwd);
                this.Close();
            }
        }
    }
}
